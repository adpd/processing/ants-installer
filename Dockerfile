# syntax=docker/dockerfile:1
FROM ubuntu:22.04
COPY ANTs /opt/ANTs
WORKDIR /opt/ANTs
ENV ANTSPATH=/opt/ANTs/bin
ENV PATH=${ANTSPATH}:PATH
ENTRYPOINT ["/opt/ANTs/bin/antsRegistration"]
