#!/bin/bash

antsBuildInstructions="https://github.com/ANTsX/ANTs/wiki/Compiling-ANTs-on-Linux-and-Mac-OS"

# Number of threads used by make
buildThreads=16

# ANTs version to install
antsVersion=master
#antsVersion=v2.3.5 # 2.3.5 doesn't actually compile

workingDir=${PWD}

# Where to build, should be an empty directory
buildDir=${workingDir}/build
installDir=/opt/ANTs

echo "
This script will download ANTs $antsVersion, 
  build in $buildDir and 
  install in $installDir 

You may need to
sudo mkdir -p $installDir
sudo chown -R ${USER} $installDir

Developer tools including compilers, git and cmake must be installed

If you encounter errors, please see the installation instructions at

  $antsBuildInstructions

Build will proceed in 5 seconds, and use $buildThreads threads
"

sleep 5


# Clone the repo
git clone https://github.com/ANTsX/ANTs.git

# If you want to build a particular release, do so here
cd ANTs
git checkout $antsVersion
cd -

mkdir $buildDir $installDir

cd $buildDir

# USE_VTK must be turned on to build antsSurf
cmake \
    -DCMAKE_INSTALL_PREFIX=$installDir \
    -DBUILD_SHARED_LIBS=OFF \
    -DUSE_VTK=OFF \
    -DSuperBuild_ANTS_USE_GIT_PROTOCOL=OFF \
    -DBUILD_TESTING=OFF \
    -DRUN_LONG_TESTS=OFF \
    -DRUN_SHORT_TESTS=OFF \
    ${workingDir}/ANTs 2>&1 | tee cmake.log

if [[ $? -ne 0 ]]; then
  echo "ANTs SuperBuild configuration failed. Please review documentation at

    $antsBuildInstructions

  If opening an issue, please attach
  
  ${buildDir}/cmake.log
  ${buildDir}/CMakeCache.txt
  ${buildDir}/CMakeFiles/CMakeError.log
  ${buildDir}/CMakeFiles/CMakeOutput.log
  
"
  exit 1 
fi

make -j $buildThreads 2>&1 | tee build.log

if [[ ! -f "CMakeFiles/ANTS-complete" ]]; then
  echo "ANTs compilation failed. Please review documentation at

    $antsBuildInstructions

  If opening an issue, please attach

  ${buildDir}/build.log
  ${buildDir}/cmake.log
  ${buildDir}/CMakeCache.txt
  ${buildDir}/CMakeFiles/CMakeError.log
  ${buildDir}/CMakeFiles/CMakeOutput.log
  
"
  exit 1
fi

cd ANTS-build
make install 2>&1 | tee install.log

antsRegExe="${installDir}/bin/antsRegistration"

if [[ ! -f ${antsRegExe} ]]; then
  echo "Installation failed. Please review documentation at

    $antsBuildInstructions

  If opening an issue, please attach

  ${buildDir}/build.log
  ${buildDir}/cmake.log
  ${buildDir}/CMakeCache.txt
  ${buildDir}/ANTS-build/install.log
  ${buildDir}/CMakeFiles/CMakeError.log
  ${buildDir}/CMakeFiles/CMakeOutput.log

"
  exit 1
fi

echo "Installation complete, running ${antsRegExe}"

${antsRegExe} --version

echo "
Binaries and scripts are located in 

  $installDir

Please see post installation instructions at 

  $antsBuildInstructions

"
