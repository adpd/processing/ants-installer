# ANTs

Shell project to dockerize ANTs Registration

## Build

Copy the `ANTs/install` dir to `./ANTs`, the build

``` bash
cp -R ~/ANTs/install ./ANTs
docker build -t antsregistration
docker tag 3mblay/antsregistration:latest
docker push
```

## Run

``` bash
docker run 3mblay/antsregistration --version
```
